const Book = require('../../../backend/routes/book/book');

describe('Books', function () {
  describe('when creating books', function () {
    let book,
      router;

    beforeEach(function() {
      router = jasmine.createSpyObj('router', ['get']);
      book = new Book(router);
      book.define();
    });

    it('should define the get rule for book', function() {
      expect(router.get).toHaveBeenCalledWith('/books', jasmine.any(Function));
    });
  });

  describe('when requesting book', function() {
    let book;
    let result,
      router,
      bookAdapter,
      requestSpy,
      requestCallback;

    beforeEach(function() {
      router = jasmine.createSpyObj('router', ['get']);
      router.get.and.callFake(function(url, callback) {
        bookAdapter = callback;
      });
      book = new Book(router);
      book.define();

      requestSpy = spyOn(require('../../../backend/request/request.js'), 'get');

      requestSpy.and.callFake(function(url, callback) {
        requestCallback = callback;
      });

      result = bookAdapter({
        query: {}
      });
    });

    it('should request the url', function() {
      expect(requestSpy).toHaveBeenCalledWith('https://goo.gl/Lk2MTJ', jasmine.any(Function));
    });
  });

  describe('when the book request is successfull', function() {
    let book;
    let sendSpy,
      router,
      bookAdapter,
      requestSpy,
      requestCallback;

    beforeEach(function() {
      router = jasmine.createSpyObj('router', ['get']);
      router.get.and.callFake(function(url, callback) {
        bookAdapter = callback;
      });
      book = new Book(router);
      book.define();

      requestSpy = spyOn(require('../../../backend/request/request.js'), 'get');

      requestSpy.and.callFake(function(url, callback) {
        requestCallback = callback;
      });

      sendSpy = jasmine.createSpy('send');

      result = bookAdapter({
        query: {}
      },
        {
          send: sendSpy
        });

      var json = {
        "OLID:1231231M": {
          title: "title",
          authors: [],
          cover: {

          }
        }
      }
      requestCallback(null, null, JSON.stringify(json));
    });

    it('should return the book collection', function() {
      expect(sendSpy).toHaveBeenCalledWith({
        books: [{
          olid: '1231231M',
          title: 'title',
          authors: [],
          cover: {}
        }]
      })
    });
  });

  describe('when requesting with a query title ', function() {
    let book;
    let sendSpy,
      router,
      bookAdapter,
      requestSpy,
      requestCallback;

    beforeEach(function() {
      router = jasmine.createSpyObj('router', ['get']);
      router.get.and.callFake(function(url, callback) {
        bookAdapter = callback;
      });
      book = new Book(router);
      book.define();

      requestSpy = spyOn(require('../../../backend/request/request.js'), 'get');

      requestSpy.and.callFake(function(url, callback) {
        requestCallback = callback;
      });

      sendSpy = jasmine.createSpy('send');

      result = bookAdapter({
          query: {
            title: 'my'
          }
        },
        {
          send: sendSpy
        });

      var json = {
        "OLID:1231231M": {
          title: "title",
          authors: [],
          cover: {

          }
        },
        "OLID:1231232M": {
          title: "My title",
          authors: [],
          cover: {

          }
        }
      }
      requestCallback(null, null, JSON.stringify(json));
    });

    it('should return the book collection filtered by the title', function() {
      expect(sendSpy).toHaveBeenCalledWith({
        books: [{
          olid: '1231232M',
          title: 'My title',
          authors: [],
          cover: {}
        }]
      })
    });
  });

  describe('when requesting with a query id', function() {
    let book;
    let sendSpy,
      router,
      bookAdapter,
      requestSpy,
      requestCallback;

    beforeEach(function() {
      router = jasmine.createSpyObj('router', ['get']);
      router.get.and.callFake(function(url, callback) {
        bookAdapter = callback;
      });
      book = new Book(router);
      book.define();

      requestSpy = spyOn(require('../../../backend/request/request.js'), 'get');

      requestSpy.and.callFake(function(url, callback) {
        requestCallback = callback;
      });

      sendSpy = jasmine.createSpy('send');

      result = bookAdapter({
          query: {
            id: '1111112M'
          }
        },
        {
          send: sendSpy
        });

      var json = {
        "OLID:1111112M": {
          title: "title",
          authors: [],
          cover: {

          }
        },
        "OLID:444444M": {
          title: "My title",
          authors: [],
          cover: {

          }
        }
      };
      requestCallback(null, null, JSON.stringify(json));
    });

    it('should return the book collection filtered by the query', function() {
      expect(sendSpy).toHaveBeenCalledWith({
        books: [{
          olid: '1111112M',
          title: 'title',
          authors: [],
          cover: {}
        }]
      })
    });
  });
});