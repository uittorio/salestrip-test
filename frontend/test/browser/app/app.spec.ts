import * as React from "react";
import App from "../../../browser/app/App";
import { SInjector } from "../../../core/injector/injector";
import {shallow, mount} from 'enzyme';

const ReactTestRenderer = require('react-test-renderer');

const injector: SInjector = new SInjector();

describe("when app is loaded", () => {
	describe("when books loads", () => {
		let listMock,
			reactRendered;
		
		beforeEach(() => {
			listMock = jest.fn(() => Promise.resolve([]));
			injector.add('bookRequester', {
					list: listMock
				}
			);
			
			reactRendered = ReactTestRenderer.create(React.createElement(App, { injector: injector}));
		});
		
		it('should render the component', function () {
			expect(reactRendered.toJSON()).toMatchSnapshot();
		});
		
		it('should load the books', async() => {
			expect(listMock).toHaveBeenCalledWith('');
		});
	});
	
	describe("when books fail to load", () => {
		let listMock,
			reactRendered;
		
		beforeEach(() => {
			listMock = jest.fn(() => Promise.reject([]));
			injector.add('bookRequester', {
					list: listMock
				}
			);
			
			reactRendered = ReactTestRenderer.create(React.createElement(App, { injector: injector}));
		});
		
		it('should still render the component', function () {
			expect(reactRendered.toJSON()).toMatchSnapshot();
		});
		
		it('should try to load the books', async() => {
			expect(listMock).toHaveBeenCalledWith('');
		});
	});
});