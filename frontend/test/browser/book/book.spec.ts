import * as React from "react";
import { BookComponent } from "../../../browser/book/Book";
import { BookFake } from "../../core/book/book.fake";

const ReactTestRenderer = require('react-test-renderer');


describe("when books are fetched", () => {
	it('should display the information', async() => {
		const reactRendered = ReactTestRenderer.create(React.createElement(BookComponent, { book: new BookFake() }));
		
		let tree = reactRendered.toJSON();
		expect(tree).toMatchSnapshot();
	});
	
	it('should display authors', async() => {
		const reactRendered = ReactTestRenderer.create(React.createElement(BookComponent, { book: new BookFake([
				{
					name: 'authorName',
					url: 'authorUrl'
				}
			]) }));
		
		let tree = reactRendered.toJSON();
		expect(tree).toMatchSnapshot();
	});
});