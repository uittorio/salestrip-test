import { BookRequester } from "../../../core/bookRequester/bookRequester";
import { Book } from "../../../core/book/book";
import { BookListData } from "../../../core/book/bookListData";
import { BookData } from "../../../core/book/bookData";
jest.mock('../../../core/book/book');

describe("BookRequester", () => {
	let bookRequester: BookRequester,
		bookData: BookData;
	
	beforeEach(() => {
		bookData = {
			olid: 'id',
			title: 'title',
			authors: [],
			cover: {
				small: 'covers',
				medium: 'coverm',
				large: 'coverl'
			}
		};
		
		window.fetch = jest.fn().mockImplementation(() => {
			return new Promise((resolve) => {
				resolve({
					json: (): BookListData => {
						return {
							books: [
								bookData,
								bookData
							]
						}
					}
				});
			});
		});
		
		bookRequester = new BookRequester();
	});
	
	beforeEach(() => {
		(Book.create as any).mockClear();
		Book.create = jest.fn(() => {
			return {
				id: '1'
			}
		});
	});
	
	it('should request books', () => {
		bookRequester.list('');
		expect(window.fetch).toHaveBeenCalledWith('/books');
	});
	
	it('should request books with the olid id params', () => {
		bookRequester.list('OL123123M');
		expect(window.fetch).toHaveBeenCalledWith('/books?id=OL123123M');
	});
	
	it('should request books with the id', () => {
		bookRequester.list('OL123123');
		expect(window.fetch).toHaveBeenCalledWith('/books?id=OL123123');
	});
	
	it('should request books without params', () => {
		bookRequester.list('');
		expect(window.fetch).toHaveBeenCalledWith('/books');
	});
	
	it('should return the books', async () => {
		const response = await bookRequester.list('');
		expect(response).toEqual([
			{
				id: '1'
			},
			{
				id: '1'
			}
		]);
	});
	
	it('should create a list of books', async () => {
		await bookRequester.list('');
		expect(Book.create).toHaveBeenCalledWith(bookData);
		expect((Book.create as any).mock.calls.length).toBe(2);
	});
});
