import { Book } from "../../../core/book/book";
import { BookDataAuthor } from "../../../core/book/bookDataAuthor";

export class BookFake extends Book {
	constructor(authors: Array<BookDataAuthor> = []) {
		super({
			olid: 'fakeId',
			title: 'fakeTitle',
			authors: authors,
			cover: {
				small: 'fakecovers',
				medium: 'fakecoverm',
				large: 'fakecovers'
			}
		})
	}
}