import { Book } from "../../../core/book/book";

describe("Book", () => {
	let book: Book;
	
	beforeEach(() => {
		book = Book.create({
			olid: 'id',
			title: 'title',
			authors: [
				{
					name: 'author a',
					url: 'author/url'
				}
			],
			cover: {
				medium: 'coverm',
				small: 'covers',
				large: 'coverl'
			}
		});
	});
	
	it('should have an id', () => {
		expect(book.id).toBe('id');
	});
	
	it('should have a title', () => {
		expect(book.title).toBe('title');
	});
	
	it('should expose the name of authors', () => {
		expect(book.authors).toEqual(['author a']);
	});
	
	it('should expose the medium cover', () => {
		expect(book.cover).toBe('coverm');
	});
	
	describe("when books author are not defined", () => {
		it('should expose an empty array of authors', function () {
			const bookWithoutAuthor: Book = Book.create({
				olid: 'id',
				title: 'title',
				cover: {
					small: 's',
					medium: 'm',
					large: 'l'
				}
			});
			
			expect(bookWithoutAuthor.authors).toEqual([]);
		});
	});
});
