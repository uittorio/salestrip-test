import './main.scss';
import * as React from "react";
import * as ReactDOM from "react-dom";
import App from "./app/App";
import { SInjector } from "../core/injector/injector";
import { BookRequester } from "../core/bookRequester/bookRequester";

const injector: Injector = new SInjector();
injector.add('bookRequester', new BookRequester());

ReactDOM.render(React.createElement(App, {
	injector
}), document.getElementById("app"));