import * as React from "react";
import { Book } from "../../core/book/book";
import "./Book.scss";

export interface BookComponentProp {
	book: Book;
}

export class BookComponent extends React.Component<BookComponentProp> {
	render() {
		const authors: Array<JSX.Element> = this.props.book.authors.map((author: string, index: number) => {
			return <span key={index}>{author}</span>;
		});
		return (
			<div className="Book">
				<img src={this.props.book.cover}/>
				<p className="Book-title">title: {this.props.book.title}</p>
				<p>authors:  {authors}</p>
				<p>id: {this.props.book.id}</p>
			</div>
		)
	}
}