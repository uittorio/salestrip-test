import * as React from "react";
import './App.scss';
import { Book } from "../../core/book/book";
import { AppProps } from "./AppProps";
import { AppState } from "./AppState";
import { BookComponent } from "../book/Book";
import { BookRequester } from "../../core/bookRequester/bookRequester";
import { ChangeEvent } from "react";
import { debounce } from "lodash";

export class App extends React.Component<AppProps, AppState> {
	private _updateQueryDebounce: (query: string) => void;
	
	constructor(props) {
		super(props);
		this.state = {
			books: [],
			query: ''
		};
		
		this._updateQueryDebounce = debounce((query: string) => this._updateQuery(query), 400);
	}
	
	private _updateQuery(query: string): void {
		this.setState({ query }, () => {
			this._getBooks();
		});
	}
	
	public render() {
		const books: Array<JSX.Element> = this.state.books.map((book: Book, index: number) => {
			return <div className="App-book" key={index}>
				<BookComponent book={book}/>
			</div>
		});
		
		return (
			<div className="App">
				<input className="App-search" type="search" onChange={(query: ChangeEvent<HTMLInputElement>) => this._updateQueryDebounce(query.target.value)}/>
				<div className="App-bookContainer">
					{books}
				</div>
			</div>
		)
	}
	
	public componentDidMount() {
		this._getBooks();
	}
	
	private _getBooks() {
		this.props.injector.get<BookRequester>('bookRequester').list(this.state.query).then((books: Array<Book>) => {
			this.setState({
				books,
			});
		}).catch((error) => {
			this.setState({
				books: []
			});
		});
	}
}

export default App;
