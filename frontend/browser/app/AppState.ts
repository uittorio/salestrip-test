import { Book } from "../../core/book/book";

export interface AppState {
	books: Array<Book>;
	query: string;
}
