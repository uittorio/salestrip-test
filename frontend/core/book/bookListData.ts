import { BookData } from "./bookData";

export interface BookListData {
	books: Array<BookData>;
}
