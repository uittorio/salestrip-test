import { BookData } from "./bookData";
import { BookDataAuthor } from "./bookDataAuthor";

export class Book {
	public id: string;
	public title: string;
	public authors: Array<string>;
	public cover: string;
	
	public static create(data: BookData): Book {
		return new Book(data);
	}
	
	constructor(data: BookData) {
		this.id = data.olid;
		this.title = data.title;
		if (data.authors) {
			this.authors = data.authors.map((authors: BookDataAuthor) => {
				return authors.name;
			});
		} else {
			this.authors = [];
		}
		
		this.cover = data.cover.medium;
	}
}