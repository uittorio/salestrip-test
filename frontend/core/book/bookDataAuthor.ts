export interface BookDataAuthor {
	name: string;
	url: string;
}