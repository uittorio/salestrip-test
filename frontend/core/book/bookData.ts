import { BookDataAuthor } from "./bookDataAuthor";

export interface BookData {
	olid: string;
	title: string;
	authors?: Array<BookDataAuthor>;
	cover: {
		small: string;
		medium: string;
		large: string;
	}
}