import { Book } from "../book/book";
import { BookListData } from "../book/bookListData";
import { BookData } from "../book/bookData";

export class BookRequester {
	public list(query: string): Promise<Array<Book>> {
		const params: {[key: string]: string } = {
			id: this._hasOlidId(query) ? query : '',
			title: !this._hasOlidId(query) ? query: ''
		};
		
		const url: string = this._addParamsToUrl(params, '/books');
		
		return fetch(url).then((response) => response.json()).then((data: BookListData) => {
			return data.books.map((book: BookData) => {
				return Book.create(book);
			});
		});
	}
	
	private _hasOlidId(query: string): boolean {
		return query.startsWith('OL');
	}
	
	private _addParamsToUrl(params, url) {
		Object.keys(params).forEach((param: string) => {
			if (params[param]) {
				url += '?'+ param + '=' + params[param];
			}
		});
		return url;
	}
}