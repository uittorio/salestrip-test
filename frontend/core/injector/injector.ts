export class SInjector implements Injector {
	private _injectorList: Array<any> = [];
	public add(key: string, injector: any) {
		this._injectorList[key] = injector;
	}
	
	public get(key: string) {
		return this._injectorList[key];
	}
}