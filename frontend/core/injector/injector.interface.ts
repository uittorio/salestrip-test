interface Injector {
	add<T>(key: string, injector: T): void;
	get<T>(key: string): T;
}