const request = require('../../request/request.js');

class Book {
  constructor(router) {
    this._router = router;
  }

  define() {
    this._get();
  }

  _get() {
    this._router.get('/books', (req, res) => this._getBooksData(req, res));
  }

  _getBooksData(req, res) {
    const title = req.query ? req.query.title : '';
    const id = req.query ? req.query.id : '';

    request.get('https://goo.gl/Lk2MTJ', (error, response, body) => {
      const booksJson = JSON.parse(body);
      let books = Object.keys(booksJson).map((book) => this._adaptData(book, booksJson));

      if (title) {
        books = this._filterByTitle(books, title);
      }

      if (id) {
        books = this._filterById(books, id);
      }

      res.send({ books });
    })
  }

  _adaptData(book, books) {
    return {
      olid: book.replace('OLID:', ''),
      title: books[book].title,
      authors: books[book].authors,
      cover: books[book].cover
    }
  }

  _filterByTitle(books, title) {
    return books.filter((book) => {
      return book.title.toLowerCase().indexOf(title.toLowerCase()) > -1;
    });
  }

  _filterById(books, id) {
    return books.filter((book) => {
      return book.olid.toLowerCase().indexOf(id.toLowerCase()) > -1;
    });
  }
}

module.exports = Book;