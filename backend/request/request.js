const request = require('request');

function myRequest(url, callback) {
  return request(url, callback);
}

module.exports.get = myRequest;