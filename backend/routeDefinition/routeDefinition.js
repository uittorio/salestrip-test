const request = require('request');

class RouteDefinition {
  add(definition) {
    definition.define();
  }
}

module.exports = RouteDefinition;