const express = require('express');

class Router {
	constructor(currentExpress) {
		this._express = currentExpress;
		this._expressRouter = express.Router();
	}
	
	get(url, callback) {
    this._expressRouter.get(url, (req, res) => callback(req, res));
    this._express.use('', this._expressRouter);
	}
}

module.exports = Router;