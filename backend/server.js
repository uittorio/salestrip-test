const express = require('express');
const webpackConfig = require('../config/webpack.dev.config');
const webpack = require('webpack');
const Router = require('./router/router');
const Book = require('./routes/book/book');
const RouteDefinition = require('./routeDefinition/routeDefinition');

const port = 3000;
const app = express();
const compiler = webpack(webpackConfig);

app.use(require('webpack-dev-middleware')(compiler, {
  noInfo: true,
  publicPath: webpackConfig.output.publicPath
}));

app.listen(port, function (error) {
  if (error) {
    console.log(error);
  }
});

const route = new Router(app);
const routeDefinition = new RouteDefinition();
const book = new Book(route);

routeDefinition.add(book);