/*globals require, module */

const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');

module.exports = {
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        loaders: [
          'awesome-typescript-loader'
        ]
      },
      {
        test: /\.scss$/,
        loader: 'style-loader!raw-loader!sass-loader'
      }
    ]
  },
  entry: {
    app: ['./frontend/browser/main.ts']
  },
  resolve: {
    extensions: ['.ts', ".tsx", ".js"]
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: 'frontend/browser/index.html'
    }),
    new CopyWebpackPlugin([
      {
        from: 'frontend/resources',
        to: 'resources'
      }
    ])
  ]
};
